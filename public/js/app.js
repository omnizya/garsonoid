// Declares the initial angular module "meanMapApp". Module grabs other controllers and services.
var app = angular.module('teamap', ['addCtrl', 'geolocation', 'gservice', 'ui.gravatar']);